FROM theyoctojester/yocto-devcontainer:latest

ADD sdk.sh $HOME/sdk.sh
RUN $HOME/sdk.sh -y && \
    rm $HOME/sdk.sh && \
    ln -s /opt/poky/3.4/environment-setup-* .bashrc.d/